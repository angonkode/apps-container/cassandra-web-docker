FROM ruby:2.4

RUN gem install cassandra-web
COPY entrypoint.sh /
RUN chmod a+x /entrypoint.sh

EXPOSE 3000
ENTRYPOINT ["/entrypoint.sh"]

