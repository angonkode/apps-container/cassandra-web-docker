#!/bin/bash


function join_by {
  local IFS="$1"
  shift
  echo "$*"
}

SLEEP_WAIT=20
CASSANDRA_OPTS=""

if [ -v CASSANDRA_PORT ]; then
  CASSANDRA_OPTS="$CASSANDRA_OPTS --port $CASSANDRA_PORT"
fi

if [ -v CASSANDRA_USER ]; then
  CASSANDRA_OPTS="$CASSANDRA_OPTS --username $CASSANDRA_USER"
fi

if [ -v CASSANDRA_PASSWORD ]; then
  CASSANDRA_OPTS="$CASSANDRA_OPTS --password $CASSANDRA_PASSWORD"
fi

if [ ! -v CASSANDRA_HOST ]; then
  sleep $SLEEP_WAIT
  exit 1
fi

if [ "XX" == "XX$CASSANDRA_HOST" ]; then
  sleep $SLEEP_WAIT
  exit 1
fi

HOST_PARAM=$(eval echo $CASSANDRA_HOST)
HOSTS=$(for a in $HOST_PARAM; do getent hosts $a | while read x y; do echo $x; done; done)
CASSANDRA_IP=$(join_by , $HOSTS)

if [ "XX" == "XX$CASSANDRA_IP" ]; then
  sleep $SLEEP_WAIT
  exit 1
fi

COMMAND="cassandra-web --hosts $CASSANDRA_IP $CASSANDRA_OPTS"
exec $COMMAND

