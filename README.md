# cassandra-web-docker

Dockerized [cassandra-web](https://github.com/avalanche123/cassandra-web)

[sample docker compose](docker-compose.yml)

### Project builds

login to docker hub
```
docker login
```

build container
```
cd <project working directort>
docker build -t angonkomputer/cassandra-web-docker:latest .
```

upload container
```
docker push angonkomputer/cassandra-web-docker:latest
```
